# Olá! Bem-vindo(a) à disciplina de Banco de Dados!

Nesta disciplina, você aprenderá sobre conceitos fundamentais de banco de dados, como modelagem de dados, normalização, linguagem SQL, arquitetura de sistemas de banco de dados e técnicas de otimização de desempenho.

Para acompanhar as aulas e atividades da disciplina, recomendamos que você crie uma conta no GitHub, uma plataforma de hospedagem de código-fonte e arquivos para controle de versão. Isso permitirá que você acesse os materiais da disciplina, faça download dos arquivos necessários e submeta suas soluções de atividades e projetos.

Ao longo do curso, você terá a oportunidade de desenvolver projetos práticos, utilizando ferramentas populares de banco de dados, como MySQL, PostgreSQL e MongoDB. Além disso, você será desafiado(a) a aplicar seus conhecimentos para resolver problemas reais, como modelar e implementar um sistema de gerenciamento de vendas para uma loja virtual.

Esperamos que você aproveite ao máximo esta disciplina e esteja pronto(a) para explorar o fascinante mundo dos bancos de dados!
